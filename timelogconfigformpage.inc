<?php
/**
 * @file
 * Includefile for timelog.
 */

/**
 * The configuration form
 */
function timelogconfig_form($form, &$form_submit) {
  $od = variable_get('timelogregtype');
  $rolesoptions = timelog_getroles();
  $form['timelogregtype'] = array(
    '#type' => 'select',
    '#title' => t('New config: Type of log'),
    '#options' => array(
      0 => t('No logging'),
      1 => t('Log only first and last login/logout per date'),
      2 => t('Log all logins/logouts'),
    ),
    '#default_value' => $od,
    '#description' => t('Log only first/last login/logout per date <br />
      OR<br /> Log all login/logout (possibly generates lots of records)'),
  );
  $form['timelogregrole'] = array(
    '#prefix' => '<span class="right_arrow"></span>',
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Roles to log'),
    '#options' => $rolesoptions,
    '#description' => t('Which roles do you want to log, <br />
      you can select more than one.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Validate the form.
 */
function timelogconfig_form_validate($form, &$form_state) {
  $regchoice = $form_state['values']['timelogregtype'];
  $rolechoice = $form_state['values']['timelogregrole'];
  if (($regchoice > 0) && (!$rolechoice)) {
    form_set_error('timelogregrole', t('You want to log<br />You have to choose role(s) to log'));
  }
  if (($regchoice == 0) && ($rolechoice)) {
    form_set_error('timelogregtype', t('You selected roles to log<br />You have to choose type of log'));
  }
}


/**
 * Page for embedding the configuration form on the page.
 */
function timelogconfigformpage() {
  $od = variable_get('timelogregtype');
  $a1 = array('No logging', 'Log only first and last login/logout per date',
    'Log all logins/logouts');
  $f = variable_get('timelogregroles');
  $l = '';
  foreach ($f as $k => $v) {
    $l .= $v . ' - ';
  }
  $info = '<p>CURRENT CONFIGURATION:<br />Type: ' . drupal_strtoupper($a1[$od]) .
    '<br />Roles: ' . drupal_strtoupper($l) .
    '<br />REMEMBER TO CLEAN CACHE AFTER CHANGING CONFIGURATION</p>';
  $thepage = array(
    'header_text' => array(
      '#type' => 'markup',
      '#markup' => '<p>' . $info . '</p>',
    ),
    'theform' => drupal_get_form('timelogconfig_form'),
  );
  return $thepage;
}

/**
 * Registrate configuration
 */
function timelogconfig_form_submit($form, &$form_state) {
  $regchoice = $form_state['values']['timelogregtype'];
  variable_set('timelogregtype', $regchoice);

  $rolesoptions = timelog_getroles();
  $rolechoice = array();
  $rolechoice = $form_state['values']['timelogregrole'];
  $i = 0;
  $rolevar = array();
  foreach ($rolechoice as $k => $v) {
    $rolevar[$i] = $rolesoptions[$v];
    $i++;
  }
  variable_set('timelogregroles', $rolevar);
}

/**
 * Get available roles
 */
function timelog_getroles() {
  $options = array();
  $results = db_query("SELECT rid, name FROM {role}");
  foreach ($results as $option) {
    $options[$option->rid] = $option->name;
  }
  return $options;
}
