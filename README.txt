TimeLog README.txt

TimeLog = a simple user timelog module for Drupal 7
Created by cord.no - dmaster.cord.no

1 SUMMARY - WHAT IT DOES
2 REQUIREMENTS - DEPENDENCIES
3 INSTALLATION
4 CONFIGURATION
5 PERMISSIONS
6 REPORTS - VIEWS
7 UNINSTALL
8 CONTACT - FEEDBACK


1 SUMMARY - WHAT IT DOES

* How much time are you and other users on the site?
* TimeLog register users login and logout time.
* TimeLog calculates users logged in/out time and then showing possible 
  worked hours.

* Using Views module you can create informative reports for each user.

* TimeLog can be configured for roles to:
  a) Log users first and last login during a date.
  b) Log all login and logouts
  c) Which roles to log.


2 REQUIREMENTS - DEPENDENCIES
Drupal 7 core
PHP 5.x

This module are dependent on following modules
date_api
date
date_popup
references
user_reference
entity_token
entity
access control


3 INSTALLATION
Download the module and unpack it in your module directory.
Now you find the module in Admin > Modules > CORD.
Enable TimeLog. 

If module "Conntent access" is not installed before enabling timelog, 
follow instructions to Rebuild permissions
Administration » Configuration » Development » Logging and errors

If module "Date" is not installed before enabling timelog, follow instructions 
Administration » Configuration » Regional and language » Locale 
to set "first day of week"
AND
Administration » Configuration » Regional and language » Date and time 
to save "Date types"


4 CONFIGURATION
During installation TimeLog created:
content type: "timelog"
user: "timelogautouser" with administrator role.

Setting permissions on timelog nodes (see also 5 PERMISSIONS)
Administration » Structure » Content types » Manage fields
Access Control tab
Set who can view, edit and delete the nodes that timelog creates.

TimeLog configuration
As default TimeLog is set to "No logging".
Admin > Configuration > People > Configure timelog

* First choose type of log, you have 3 options:
  0 No logging
  1 Users first and last login during a date
  2 Log all logins and logouts - (can generate lots of nodes)

* Second choose which userroles to log: 
  You get a list of userroles available from the system.

TimeLog nodes
* Nodes created by TimeLog
  All nodes have the titlestandard of 
  "TimeLog(type of log) (user uid) (Date) (status)"
  ex: "TimeLog1 2 2012-04-21 i"
  status = i - the user is logged in
  status = u - the user has logged out
  

5 PERMISSIONS - THIS IS IMPORTANT!
Who can access the TimeLog nodes? You have to configure this yourself!
Who can access any views? You have configure this yourself.

Using the module "Content access" module, you can set this very specifically.
After enabling "Content access" go to:
Admin > Structure > Content types > Timelog > Edit > Access controll
Now set who can view, edit and delete TimeLog content in
Role based access control settings.
Suggestion: only administrator on all.


6 REPORTS - VIEWS
You can use views to create informative reports of users worked days and 
hours per day.


7 UNINSTALL
If you uninstall TimeLog, all records created by the module and all 
configuration info concerning TimeLog is deleted.


8 FEEDBACK
Constructive feedback are welcome. 

Current maintainers:
* Ole Roger Dahle (cord1) - http://drupal.org/user/837398

This project has been sponsored by:
* CORD
  Certified project management, certified programmers, database developers and 
  specialized on Drupal.
  Visit http://cord.no OR http://dmaster.cord.no for more information.
